implementation module Data.GenType.CCode

import StdEnv
import Data.Either
import Data.Func
import Data.Functor
from Data.List import lookup
from Data.Map import :: Map(..), fromList, get, newMap
import Data.Maybe
import Control.Applicative
import Control.Monad
from Text import class Text(concat,toUpperCase), instance Text String

import Data.GenType
import Data.GenType.CCode.CType
import Data.GenType.CCode.CParser
import Data.GenType.CCode.CPrinter

instance zero GenCCodeOptions
where
	zero =
		{ basename   = ""
		, genParser  = True
		, genPrinter = True
		, customDeps = id
		, overrides  = []
		}
instance zero CCState
where
	zero = {prefixMap = newMap, enums=[]}

withPrefixMap :: ((Map String (String, Bool)) -> Map String (String, Bool)) CCState -> CCState
withPrefixMap f s = {s & prefixMap=f s.prefixMap}

prefixMap :: CCState -> Map String (String, Bool)
prefixMap s = s.prefixMap

generateCCode :: GenCCodeOptions (Box GType a) -> Either String ([String], [String]) | gType{|*|} a
generateCCode opts box
	=         typedefs typedefMap type
	>>= \tdh->if opts.genParser (parsers parseMap type) (Right ([], []))
	>>= \(pah, pac)->if opts.genPrinter (printers printMap type) (Right ([], []))
	>>= \(prh, prc)->Right
		( flatten [toph, tdh, pah, prh, both]
		, flatten $ if (opts.genPrinter || opts.genParser)
			[[includes:topc], pac, prc, ["\n"]]
			[["\n"]]
		)
where
	both = ["#endif /* ", toUpperCase (safe opts.basename), "_H */\n"]
	toph =
		[ "#ifndef ", toUpperCase (safe opts.basename), "_H\n"
		, "#define ", toUpperCase (safe opts.basename), "_H\n"
		, includes
		, "struct _Array { uint32_t size; void **elements; };\n"
		, "struct _BngArray {struct _Array arr; };\n"
		, "void print__Array_p(void (*put)(uint8_t), void *r, void (*print_0)(void (*)(uint8_t), void *));\n"
		, "void print__Array(void (*put)(uint8_t), struct _Array r, void (*print_0)(void (*)(uint8_t), void *));\n"
		, "void *parse__Array_p(uint8_t (*get)(void), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(void), void *(*)(size_t)));\n"
		, "struct _Array parse__Array(uint8_t (*get)(void), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(void), void *(*)(size_t)));\n"
		, "#define print__BngArray(a, b, c) print__Array(a, b.arr, c)\n"
		, "#define print__BngArray_p print__Array_p\n"
		, "#define parse__BngArray(a, b, c) ((struct _BngArray){.arr=parse__Array(a, b, c)})\n"
		, "#define parse__BngArray_p parse__Array_p\n"
		, "\n"
		]
	topc =
		[ "#include \"", opts.basename, ".h\"\n"
		, "union floatint {uint32_t intp; float floatp; };\n"
		, "union doubleint {uint64_t intp; double doublep; };\n"
		, "void print__Array_p(void (*put)(uint8_t), void *r, void (*print_0)(void (*)(uint8_t), void *))\n"
		, "{\n"
		, "	print__Array(put, *(struct _Array *)r, print_0);\n"
		, "}\n"
		, "void print__Array(void (*put)(uint8_t), struct _Array r, void (*print_0)(void (*)(uint8_t), void *))\n"
		, "{\n"
		, "	put(r.size>>24);\n"
		, "	put(r.size>>16);\n"
		, "	put(r.size>>8);\n"
		, "	put(r.size & 0xff);\n"
		, "	for (uint32_t i = 0; i<r.size; i++) {\n"
		, "		print_0(put, r.elements[i]);\n"
		, "	}\n"
		, "}\n"
		, "void *parse__Array_p(uint8_t (*get)(void), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(void), void *(*)(size_t)))\n"
		, "{\n"
		, "	struct _Array *r = (struct _Array *)alloc(sizeof(struct _Array));\n"
		, "\n"
		, "	*r = parse__Array(get, alloc, parse_0);\n"
		, "\n"
		, "	return r;\n"
		, "}\n"
		, "struct _Array parse__Array(uint8_t (*get)(void), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(void), void *(*)(size_t)))\n"
		, "{\n"
		, "	struct _Array r;\n"
		, "	r.size = (uint32_t)get()<<24;\n"
		, "	r.size += (uint32_t)get()<<16;\n"
		, "	r.size += (uint32_t)get()<<8;\n"
		, "	r.size += (uint32_t)get();\n"
		, "	r.elements = alloc(r.size*sizeof(void *));\n"
		, "	for (uint32_t i = 0; i<r.size; i++) {\n"
		, "		r.elements[i] = parse_0(get, alloc);\n"
		, "	}\n"
		, "	return r;\n"
		, "}\n"
		]
	includes = "#include <stdint.h>\n#include <stdbool.h>\n#include <stddef.h>\n"
	typedefMap = fromList [(n, t)\\(n, t, _, _)<-opts.overrides]
	parseMap   = fromList [(n, p)\\(n, _, p, _)<-opts.overrides]
	printMap   = fromList [(n, p)\\(n, _, _, p)<-opts.overrides]
	type = opts.customDeps $ map (map gTypeToType) $ flattenGType $ unBox box

safe :: String -> String
safe s = concat [fromMaybe {c} (lookup c cs)\\ c<-:s]

cs :: [(Char, String)]
cs = //fromList
	[('~', "Tld"), ('@', "At"), ('#', "Hsh"), ('$', "Dlr"), ('%', "Prc")
	,('^', "Hat"), ('?', "Qtn"), ('!', "Bng"), (':', "Cln"), ('+', "Pls")
	,('-', "Min"), ('*', "Ast"), ('<', "Les"), ('>', "Gre"), ('\\', "Bsl")
	,('/', "Slh"), ('|', "Pip"), ('&', "Amp"), ('=', "Eq"), ('.', "Dot")
	,('{', "Cbo"), ('}', "Cbc")]

prefix :: Type -> String
prefix (TyRecord _ _) = "struct "
prefix (TyArray _ _) = "struct "
prefix (TyObject _ fs)
	| and [t =: [] \\ (_, t)<-fs] = "enum "
	| fs =: [(_, [_])] = ""
	| fs =: [_] = "struct "
	= "struct "
prefix  _ = ""

consName :: GenericTypeDefDescriptor -> String
consName s = "enum " +++ safe s.gtd_name
