definition module Data.GenType.CCode.CType

from Data.Map import :: Map
from StdGeneric import :: GenericTypeDefDescriptor
from Data.Either import :: Either
from Data.GenType import :: Type

/**
 * generate typedefs for the types grouped by strongly connected components
 *
 * @param predefined types
 * @param types to typedef
 * @result typedefs or error
 */
typedefs :: (Map String [String]) [[Type]] -> Either String [String]

/**
 * Generate a single typedef for a type.
 * This does not terminate for recursive types
 *
 * @param predefined types
 * @param type to typedef
 * @result typedef or error
 */
flatTypedef :: (Map String [String]) Type -> Either String [String]
