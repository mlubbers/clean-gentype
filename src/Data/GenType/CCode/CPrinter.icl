implementation module Data.GenType.CCode.CPrinter

import StdEnv
import Data.Tuple
import Data.Functor
import Data.Functor.Identity
import Data.Func
import Data.List
import Data.Maybe
import qualified Data.Map
from Data.Map import :: Map(..)
import Data.Monoid
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans
import Control.Monad.Fail
import Control.Monad.Reader
import Control.Monad.Writer
import Data.Either

import Data.GenType.CCode
import Data.GenType

instance MonadFail (Either String) where fail s = Left s
:: FPMonad :== WriterT [String] (ReaderT Int (Either String)) ()

indent c = liftT ask >>= \i->tell [createArray i '\t':c]
(<.>) infixr 6
(<.>) a b = a +++ "." +++ b

(<->) infixr 6
(<->) a b = a +++ "." +++ b

tail = ["}\n"]
ctypename t c = [prefix t, safe (typeName t):c]
printfun t c = ["print_", safe (typeName t):c]

double2int r = "(union doubleint) {.doublep=" +++ r +++ "}.intp"
putint put bytes r = sequence_ [put ["put ((", r, ">>", toString (i*8), ") & 0xff);\n"]\\i<-reverse [0..bytes-1]]

flatPrinter :: (Map String (String -> [String])) Type -> Either String ([String], [String])
flatPrinter tmap t = tuple (printdef [";\n"]) <$> runReaderT (execWriterT $
	tell (printdef ["{\n"]) >>| fpd t True "r" >>| tell tail) 1
where
	printdef c = ["void ":printfun t ["(void (*put)(uint8_t), ":ctypename t [" r)":c]]]

	fpd :: Type Bool String -> FPMonad
	fpd t tl r | 'Data.Map'.member (typeName t) tmap
		= mapM_ (\x->indent [x]) $ 'Data.Map'.find (typeName t) tmap r
	fpd (TyRef s) tl r = fail "flatPrinter failed because of a reference"
	fpd (TyBasic t) tl r
		| tl = pure ()
		= case t of
			BTInt = putint indent (IF_INT_64_OR_32 8 4) r
			BTChar = indent ["put(", r, ");\n"]
			BTReal = putint indent 8 (double2int r)
			BTBool = indent ["put(", r, ");\n"]
			t = fail $ "flatPrinter: there is no basic type for " +++ toString t
	fpd (TyArrow _ _) tl r = fail $ "flatPrinter: function cannot be serialised"
	fpd (TyNewType ti ci a) tl r = fpd a tl r
	fpd (TyArray _ a) tl r
		=   fpd (TyBasic BTInt) False (r <.> "size")
		>>| (\i->"i" +++ toString i) <$> liftT ask
		>>= \i->indent ["for (uint32_t ", i, " = 0; ", i, "<", r <.> "size", ";", i, "++) {\n"]
		>>| mapWriterT (local inc) (fpd a False (r <.> "elements[" +++ i +++ "]"))
		>>| indent ["}\n"]
	fpd (TyRecord ti fs) tl r
		= mapM_ fmtField [(r <.> safe fi.gfd_name, ty)\\(fi, ty)<-fs]
	//Enumeration
	fpd (TyObject ti fs) tl r
		| and [t =: [] \\ (_, t)<-fs]
			= indent ["put(", r, ");\n"]
	//Single constructor, single field (box)
	fpd (TyObject ti [(ci, [ty])]) tl r = fpd ty tl r
	//Single constructor
	fpd (TyObject ti [(ci, ts)]) tl r
		=   mapM_ fmtField [(r <.> "f" +++ toString i, ty)\\i<-[0..] & ty<-ts]
	//Complex adt
	fpd (TyObject ti fs) tl r
		=   indent ["put(", r <.> "cons);\n"]
		>>| indent ["switch (", r <.> "cons){\n"]
		>>| mapM_ fmtCons fs
		>>| indent ["}\n"]
	where
		fmtCons :: (GenericConsDescriptor,[Type]) -> FPMonad
		fmtCons (ci, ts) = indent ["case ", safe ci.gcd_name, "_c:\n"]
			>>| mapM_ (mapWriterT (local inc) o fmtField) [(cs i, ty) \\i<-[0..] & ty<-ts]
			>>| mapWriterT (local inc) (indent ["break;\n"])
		where
			cs i = r <.> "data" <.> safe ci.gcd_name +++ if (ts=:[_]) "" ("" <.> "f" +++ toString i)
	fpd t tl r = fail $ "flatParser: unsupported " +++ toString t

	fmtField :: (String, Type) -> FPMonad
	fmtField (name, ty) = fpd ty False name

:: TPMonad a :== WriterT [String] (StateT CCState (Either String)) a
printers :: (Map String (String -> [String])) [[Type]] -> Either String ([String], [String])
printers tmap ts = tuple (printdefs ts) <$> evalStateT (execWriterT $ mapM_ printergroup ts) zero
where
	printdef_p :: Type [String] -> [String]
	printdef_p t c = ["void ":printfun t ["_p(void (*put)(uint8_t), void *r":pks (typeKind t) True [")":c]]]

	printdef :: Type [String] -> [String]
	printdef t c = ["void ":printfun t ["(void (*put)(uint8_t), ":ctypename t [" r":pks (typeKind t) True [")":c]]]]

	isPointer tname = liftT $ gets $ maybe False snd o 'Data.Map'.get tname o prefixMap

	pks :: Kind Bool [String] -> [String]
	pks k tl c = foldr (\(i, k) c->pd k tl i c) c $ zip2 [0..] $ typeArgs k

	pd :: Kind Bool Int [String] -> [String]
	pd KStar tl i c = [", void (*", if tl ("print_"+++toString i) "", ")(void (*)(uint8_t), void *)":c]
	pd (l KArrow r) tl i c =
		[ ", void (*", if tl ("print_"+++toString i) "", ")(void (*)(uint8_t), void *"
		: pks l False $ pd r False (inc i) [")":c]]

	typeArgs :: Kind -> [Kind]
	typeArgs KStar = []
	typeArgs (l KArrow r) = [l:typeArgs r]

	printdefs :: ([[Type]] -> [String])
	printdefs = foldr (\t c->printdef_p t [";\n":printdef t [";\n":c]]) [] o flatten

	printergroup :: [Type] -> TPMonad ()
	printergroup ts
		=   putPrefixMap [(typeName ty, (prefix ty, True))\\ty<-ts]
		>>| mapM_ funs ts
		>>| putPrefixMap [(typeName ty, (prefix ty, False))\\ty<-ts]
	where
		funs t = tell (printdef t [" {\n"]) >>| printer t >>| tell ["\n":tail]
			>>| tell (printdef_p t [" {\n"]) >>| printer_p t tail
		printer_p t c = tell ["\t":printfun t ["(put, *(":ctypename t [" *)r"
			:foldr (\i c->[", print_", toString i:c]) [");\n":c]
				$ indexList $ typeArgs $ typeKind t]]]
		putPrefixMap list = liftT $ modify $ withPrefixMap $ 'Data.Map'.putList list

	printer :: Type -> TPMonad ()
	printer t | 'Data.Map'.member (typeName t) tmap
		= mapM_ (\x->tell ["\t", x]) $ 'Data.Map'.find (typeName t) tmap "r"
	printer t=:(TyRef _) = tell $ printfun t []
	printer (TyBasic t)
		= case t of
			BTInt = putint (\t->tell ["\t":t]) (IF_INT_64_OR_32 8 4) "r"
			BTChar = tell ["\tput(r);\n"]
			BTBool = tell ["\tput(r);\n"]
			BTReal = putint (\t->tell ["\t":t]) 8 (double2int "r")
			t = fail $ "printer: there is no basic type for " +++ toString t
	printer (TyArrow _ _) = fail $ "printer: function cannot be serialized"
	printer (TyNewType ti ci a) = fmtFields 1 ci.gcd_type ["r"]
	printer t=:(TyArray _ _)
		=   putint (\i->tell ["\t":i]) 4 "r.size"
		>>| tell ["\tfor (uint32_t i = 0; i<r.size; i++) {\n"]
		>>| fmtFields 2 (typeGenType t) ["\tr.elements[i]"]
		>>| tell ["\t}\n"]
	printer (TyRecord ti fs)
		= fmtFields 1 ti.grd_type ["r" <-> safe fi.gfd_name\\(fi, _)<-fs]
	//Enumeration
	printer (TyObject ti fs)
		| and [t =: [] \\ (_, t)<-fs]
			= tell ["\tput(r);\n"]
	//Single constructor, single field (box)
	printer (TyObject ti [(ci, [ty])]) = fmtFields 1 ci.gcd_type ["r"]
	//Single constructor
	printer t=:(TyObject ti [(ci, ts)])
		= fmtFields 1 ci.gcd_type ["r" <-> "f" +++ toString i\\i<-indexList ts]
	//Complex adt
	printer (TyObject ti fs)
		=   tell ["\tput(r.cons);\n"]
		>>| tell ["\tswitch(r.cons) {\n"]
		>>| mapM_ fmtCons fs
		>>| tell ["\t}\n"]
	where
		fmtCons :: (GenericConsDescriptor,[Type]) -> TPMonad ()
		fmtCons (ci, ts) = tell ["\tcase ", safe ci.gcd_name, "_c:\n"]
			>>| fmtFields 2 ci.gcd_type [cs i\\i<-[0..] & ty<-ts]
			>>| tell ["\t\tbreak;\n"]
		where
			cs i = "r" <-> "data" <.> safe ci.gcd_name +++ if (ts=:[_]) "" ("" <.> "f" +++ toString i)
	printer t = fail $ "printer: unsupported type " +++ toString t

	fmtFields :: Int GenType [String] -> TPMonad ()
	fmtFields i _ [] = pure ()
	fmtFields i (GenTypeArrow l r) [x:xs]
		= tell [createArray i '\t']
		>>| fmtField l True x [] >>= tell >>| tell [");\n"] >>| fmtFields i r xs

	fmtField :: GenType Bool String [String] -> TPMonad [String]
	fmtField (GenTypeApp (GenTypeCons a) (GenTypeCons t)) tl r c
		| isMember a ["_Array", "_#Array", "_!Array"]
			= fmtField (GenTypeCons (t +++ a)) tl r c
	fmtField (GenTypeCons a) tl r c = isPointer a
		>>= \ip->pure ["print_", safe a:if tl [if ip "_p" "", "(put, ",r:c] ["_p":c]]
	fmtField (GenTypeVar a) tl r c = pure ["print_", toString a:if tl ["(put, ", r:c] ["":c]]
	fmtField t=:(GenTypeApp _ _) tl rs c = ufold t c
	where
		ufold (GenTypeApp l r) c = fmtField r False rs c >>= \c->ufold l [", ":c]
		ufold t c = fmtField t tl rs c
	//Arrow are allowed, but only if it is an overridden type
	fmtField t=:(GenTypeArrow l r) tl rs c
		| isPredef r = fmtField r tl rs c
	where
		isPredef (GenTypeArrow l r) = isPredef r
		isPredef (GenTypeCons c) = 'Data.Map'.member c tmap
		isPredef _ = False
	fmtField t ts rs c = abort ("printer:fmtField doesn't match for: " +++ toString t)
