implementation module Data.GenType.CCode.CType

import Control.Applicative
import Control.Monad
import Control.Monad.Fail
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Trans
import Control.Monad.Writer
import Data.Either
import Data.Func
import Data.Functor
import Data.List
import qualified Data.Map
from Data.Map import :: Map(..)
import Data.Maybe
import Data.Monoid
import Data.Tuple
import StdEnv
import qualified Text
from Text import class Text(concat), instance Text String

import Data.GenType
import Data.GenType.CCode

instance MonadFail (Either String) where fail s = Left s

consti ti = {ti & gtd_name=ti.gtd_name +++ "_c"}
enumt ti fs c = [consName ti, " {", 'Text'.join ", " [safe ci.gcd_name +++ "_c"\\(ci, _)<-fs], "} __attribute__ ((__packed__));\n":c]

iindent = mapWriterT $ mapStateT $ local inc
indent c = liftT (liftT ask) >>= \i->tell [createArray i '\t':c]

:: FTMonad :== WriterT [String] (StateT [(String, [String])] (ReaderT Int (Either String))) ()
flatTypedef :: (Map String [String]) Type -> Either String [String]
flatTypedef tmap t = (\(w, es)->flatten (map snd es) ++ w)
	<$> runReaderT (runStateT (execWriterT (ftd t True)) []) 0
where
	ftd :: Type Bool -> FTMonad
	ftd t tl | 'Data.Map'.member (typeName t) tmap
		= mapM_ (\x->indent [x]) $ 'Data.Map'.find (typeName t) tmap
	ftd (TyRef s) tl = indent [s]
	ftd (TyBasic t) tl
		| tl = pure ()
		= case t of
			BTInt  = indent [IF_INT_64_OR_32 "int64_t" "int32_t"]
			BTChar = indent ["char"]
			BTReal = indent ["double"]
			BTBool = indent ["bool"]
			t = fail $ "flatTypedef: there is no basic type for " +++ toString t
	ftd (TyArrow l r) tl = fail "flatTypedef: functions cannot be serialized"
	ftd (TyNewType ti ci a) tl = ftd a tl
	ftd t=:(TyArray _ a) tl
		=   indent ["struct ", if tl (safe $ typeName t) "", " {\n"]
		>>| indent ["uint32_t size;\n"]
		>>| iindent (fmtField ("*elements", a))
		>>| indent ["}", if tl ";\n" ""]
	ftd (TyRecord ti fs) tl
		= indent ["struct ", if tl (safe ti.grd_name) "", " {\n"
		] >>| mapM_ (iindent o fmtField) [(fi.gfd_name, ty)\\(fi, ty)<-fs]
		>>| indent ["}", if tl ";\n" ""]
	//Enumeration
	ftd (TyObject ti fs) tl
		| and [t =: [] \\ (_, t)<-fs]
			| tl = pure ()
			= indent [] >>| enum ti fs
	//Single constructor, single field (box)
	ftd (TyObject ti [(ci, [ty])]) tl = ftd ty tl
	//Single constructor
	ftd (TyObject ti [(ci, ts)]) tl
		=   indent ["struct ", if tl (safe ti.gtd_name) "", " {\n"]
		>>| mapM_ (iindent o fmtField) [("f" +++ toString i, ty)\\i<-[0..] & ty<-ts]
		>>| indent ["}", if tl ";\n" ""]
	//Complex adt
	ftd (TyObject ti fs) tl
		=   indent ["struct ", if tl (safe ti.gtd_name) "", " {\n"]
		>>| iindent (indent []) >>| enum (consti ti) fs >>| tell [" cons;\n"]
		>>| iindent (indent ["union {\n"])
		>>| mapM_ (iindent o iindent o fmtCons) fs
		>>| iindent (indent ["} data;\n"])
		>>| indent ["}", if tl ";\n" ""]
	where
		fmtCons (ci, []) = pure ()
		fmtCons (ci, [t]) = ftd t False >>| tell [" ", safe ci.gcd_name, ";\n"]
		fmtCons (ci, ts)
			=   indent ["struct {\n"]
			>>| mapM_ (iindent o fmtField) [("f" +++ toString i, ty)\\i<-[0..] & ty<-ts]
			>>| indent ["} ", safe ci.gcd_name, ";\n"]
	ftd t tl = fail $ "cannot flatTypedef: " +++ toString t

	enum :: GenericTypeDefDescriptor [(GenericConsDescriptor, [Type])] -> FTMonad
	enum ti fs = liftT (gets (lookup ti.gtd_name)) >>= \e->case e of
		?None = liftT (modify \s->[(ti.gtd_name, enumt ti fs []):s]) >>| enum ti fs
		?Just _ = tell [consName ti]

	fmtField :: (String, Type) -> FTMonad
	fmtField (name, ty) = ftd ty False >>| tell [" ", name, ";\n"]

:: TDMonad :== WriterT [String] (StateT CCState (Either String)) ()
typedefs :: (Map String [String]) [[Type]] -> Either String [String]
typedefs tmap ts = (\(text, {enums})->enums ++ text)
	<$> runStateT (execWriterT (mapM_ typedefgroup ts)) (withPrefixMap ('Data.Map'.putList [(a, ("struct ", False)) \\ a<-["_Array", "_!Array"]]) zero)
where
	typedefgroup :: [Type] -> TDMonad
	typedefgroup ts
		=   putPrefixMap [(typeName ty, (prefix ty, False))\\ty<-ts]
		// Print the prototypes (in case of a non-singleton component)
		>>| mapM_ (\x->printTypeName x >>| tell [";\n"]) (if (ts=:[_,_:_]) (map typeName ts) [])
		>>| putPrefixMap [(typeName ty, (prefix ty, True))\\ty<-ts]
		>>| mapM_ (\t->typedef t >>| tell ["\n"]) ts
		>>| putPrefixMap [(typeName ty, (prefix ty, False))\\ty<-ts]
	where
		putPrefixMap list = liftT $ modify $ withPrefixMap $ 'Data.Map'.putList list

	isEnum (TyObject ti fs) = and [t =: [] \\ (_, t)<-fs]
	isEnum _ = False

	printTypeName :: String -> TDMonad
	printTypeName tname
		= liftT (gets \s->'Data.Map'.get tname s.prefixMap)
		>>= \m->case m of
			?None = fail ("unknown type: " +++ tname)
			?Just (prefix, b) = tell [prefix, safe tname, if b " *" " "]

	typedef :: Type -> TDMonad
	typedef t | 'Data.Map'.member (typeName t) tmap
		= sequence_ [tell [x]\\x<-'Data.Map'.find (typeName t) tmap]
	typedef (TyRef s) = printTypeName s
	typedef (TyBasic t) = case t of
		BTInt = tell ["typedef ", IF_INT_64_OR_32 "int64_t" "int32_t", " Int;"]
		BTChar = tell ["typedef char Char;"]
		BTReal = tell ["typedef double Real;"]
		BTBool = tell ["typedef bool Bool;"]
		t = fail $ "basic type: " +++ toString t +++ " not implemented"
	typedef t=:(TyArray _ a)
		=   tell ["struct ", safe (typeName t), " {\n"]
		>>| tell ["\tuint32_t size;\n"]
		>>| tell ["\t"] >>| printTypeName (typeName a) >>| tell ["*elements;\n"]
		>>| tell ["};\n"]
	typedef t=:(TyNewType ti ci a)
		= tydef ti.gtd_name ci.gcd_type
	typedef t=:(TyRecord ti fs)
		=   tell ["struct ", safe ti.grd_name, " {\n"]
		>>| fmtFields 1 ti.grd_type [fi.gfd_name\\(fi, _)<-fs]
		>>| tell ["};\n"]
	//Enumeration
	typedef t=:(TyObject ti fs)
		| isEnum t = enum False ti fs
//		| and [t =: [] \\ (_, t)<-fs] = censor (\_->[]) (enum ti fs)
	//Single constructor, single field (box)
	typedef t=:(TyObject ti [(ci, [ty])]) = tydef ti.gtd_name ci.gcd_type
	//Single constructor
	typedef t=:(TyObject ti [(ci, ts)])
		=   tell ["struct ", safe ti.gtd_name, " {\n"]
		>>| fmtFields 1 ci.gcd_type ["f" +++ toString i\\i<-indexList ts]
		>>| tell ["};\n"]
	//Complex adt
	typedef t=:(TyObject ti fs) = tell
		["struct ", safe ti.gtd_name, " {\n\t"]
		>>| enum True (consti ti) fs >>| tell [" cons;\n\tunion {\n"]
			//, consName ti, " {", 'Text'.join ", " [safe ci.gcd_name\\(ci, _)<-fs], "} cons;\n"
		>>| mapM_ fmtCons fs
		>>| tell ["\t} data;\n};\n"]
	where
		fmtCons :: (GenericConsDescriptor, [Type]) -> TDMonad
		fmtCons (ci, []) = pure ()
		fmtCons (ci, [t]) = fmtFields 2 ci.gcd_type [safe ci.gcd_name]
		fmtCons (ci, ts) = tell ["\t\tstruct {\n"]
			>>| fmtFields 3 ci.gcd_type ["f" +++ toString i\\i<-indexList ts]
			>>| tell ["\t\t} ", safe ci.gcd_name, ";\n"]
	typedef t = fail $ toString t +++ " not implemented"

	enum :: Bool GenericTypeDefDescriptor [(GenericConsDescriptor, [Type])] -> TDMonad
	enum pr ti fs = liftT (modify \s->{s & enums=enumt ti fs s.enums})
		>>| tell (if pr [consName ti] [])

	tydef :: String GenType -> TDMonad
	tydef name (GenTypeArrow l r) = tell ["typedef "] >>| fmtField "" l >>| tell [safe name,";\n"]

	fmtFields :: Int GenType [String] -> TDMonad
	fmtFields i _ [] = pure ()
	fmtFields i (GenTypeArrow l r) [x:xs]
		= tell [createArray i '\t'] >>| fmtField x l >>| tell [";\n"] >>| fmtFields i r xs

	fmtField :: String GenType -> TDMonad
	fmtField x (GenTypeApp (GenTypeCons a) (GenTypeCons t))
		| isMember a ["_Array", "_#Array", "_!Array"]
			= fmtField x (GenTypeCons (t +++ a))
	fmtField x (GenTypeCons a) = printTypeName a >>| tell [x]
	fmtField x (GenTypeVar a) = tell ["void *",x]
	fmtField x (GenTypeApp l r) = fmtField x l
//	fmtField x t=:(GenTypeArrow _ _)
//		= mapM (fmap (concat o snd) o listen o fmtField "") (collectArgs t [])
//			>>= \[r:as]->tell [r, " (*",x,")(",'Text'.join ", " as, ")"]
//	where
//		collectArgs (GenTypeArrow l r) c = collectArgs r (c ++ [l])
//		collectArgs t c = [t:c]
	fmtField x t = abort ("printer:fmtField doesn't match for: " +++ toString t)
