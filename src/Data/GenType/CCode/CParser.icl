implementation module Data.GenType.CCode.CParser

import Control.Applicative
import Control.Monad
import Control.Monad.Fail
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Trans
import Control.Monad.Writer
import Data.Either
import Data.Func
import Data.Functor
import Data.Functor.Identity
import Data.List
import qualified Data.Map
from Data.Map import :: Map(..)
import Data.Maybe
import Data.Monoid
import Data.Tuple
import StdEnv
import qualified Text
from Text import class Text(concat), instance Text String

import Data.GenType
import Data.GenType.CCode

instance MonadFail (Either String) where fail s = Left s
:: FPMonad :== WriterT [String] (ReaderT Int (Either String)) ()

indent c = liftT ask >>= \i->tell [createArray i '\t':c]

parsefun t c = ["parse_", safe (typeName t):c]

(<.>) infixr 6
(<.>) a b = a +++ "." +++ b

(<->) infixr 6
(<->) a b = a +++ "." +++ b
//(<->) a b = a +++ "->" +++ b

result r op s = indent [r, " ", op, " ", s, ";\n"]
assign r s = result r "=" s
parsename s = "parse_" +++ safe s
tail = ["\treturn r;\n\t(void)alloc;\n}\n"]
parsenameimp t c ctypename def = def t [" {\n\t":ctypename t [" ", c, "\n\n"]]
ctypename t c = [prefix t, safe (typeName t):c]

parseint put bytes cast r = put [r, " = 0;\n"]
	>>| sequence_ [put [r, " += (", cast, ")get()<<", toString (i*8), ";\n"]\\i<-reverse [0..bytes-1]]
parse64bitdouble put r
	=   put ["do {\n"]
	>>| put ["\tuint64_t i;\n"]
	>>| parseint (\t->put ["\t":t]) 8 "uint64_t" "i"
	>>| put ["\t", r, "= (union doubleint) {.intp=i}.doublep;\n"]
	>>| put ["} while (0);\n"]

flatParser :: (Map String (String -> [String])) Type -> Either String ([String], [String])
flatParser tmap t = tuple (parsedef [";\n"]) <$> runReaderT (execWriterT (tell (parsenameimp t "r;" ctypename \_->parsedef) >>| fpd t True "r" >>| tell tail)) 1
where
	parsedef c = ctypename t [" ":parsefun t ["(uint8_t (*get)(void), void *(*alloc)(size_t))":c]]

	fpd :: Type Bool String -> FPMonad
	fpd t tl r | 'Data.Map'.member (typeName t) tmap
		= mapM_ (\x->indent [x]) $ 'Data.Map'.find (typeName t) tmap r
	fpd (TyRef s) tl r = fail "flatParser failed because of a reference"
	fpd (TyBasic t) tl r
		| tl = pure ()
		= case t of
			BTInt = parseint indent 4 "uint32_t" r
			BTChar = assign r "(char)get()"
			BTReal = parse64bitdouble indent r
			BTBool = assign r "(bool)get()"
			t = fail $ "flatParse: there is no basic type for " +++ toString t
	fpd (TyArrow _ _) tl r = fail $ "flatParser: function cannot be serialised"
	fpd (TyNewType ti ci a) tl r = fpd a tl r
	fpd (TyArray _ t) tl r = fail $ "flatParser: arrays not supported"
	fpd (TyRecord ti fs) tl r
		= mapM_ fmtField [(r <.> safe fi.gfd_name, ty)\\(fi, ty)<-fs]
	//Enumeration
	fpd (TyObject ti fs) tl r
		| and [t =: [] \\ (_, t)<-fs]
			= assign r $ "(" +++ consName ti +++ ") get()"
	//Single constructor, single field (box)
	fpd (TyObject ti [(ci, [ty])]) tl r = fpd ty tl r
	//Single constructor
	fpd (TyObject ti [(ci, ts)]) tl r
		=   mapM_ fmtField [(r <.> "f" +++ toString i, ty)\\i<-[0..] & ty<-ts]
	//Complex adt
	fpd (TyObject ti fs) tl r
		=   assign (r <.> "cons") ("(" +++ consName ti +++ "_c) get()")
		>>| indent ["switch (", r <.> "cons){\n"]
		>>| mapM_ fmtCons fs
		>>| indent ["}\n"]
	where
		fmtCons :: (GenericConsDescriptor,[Type]) -> FPMonad
		fmtCons (ci, ts) = indent ["case ", safe ci.gcd_name, "_c:\n"]
			>>| mapM_ (mapWriterT (local inc) o fmtField) [(cs i, ty) \\i<-[0..] & ty<-ts]
			>>| mapWriterT (local inc) (indent ["break;\n"])
		where
			cs i = r <.> "data" <.> safe ci.gcd_name +++ if (ts=:[_]) "" (".f" +++ toString i)
	fpd t tl r = fail $ "flatParser: unsupported " +++ toString t

	fmtField :: (String, Type) -> FPMonad
	fmtField (name, ty) = fpd ty False name

:: TPMonad a :== WriterT [String] (StateT CCState (Either String)) a
parsers :: (Map String (String -> [String])) [[Type]] -> Either String ([String], [String])
parsers tmap ts = tuple (parsedefs ts) <$> evalStateT (execWriterT $ mapM_ parsergroup ts) zero
where
	parsedefs :: ([[Type]] -> [String])
	parsedefs = foldr (\t c->parsedef_p t [";\n":parsedef t [";\n":c]]) [] o flatten

	parsedef_p :: Type [String] -> [String]
	parsedef_p t c = ["void *":parsefun t ["_p(uint8_t (*get)(void), void *(*alloc)(size_t)":pks (typeKind t) True [")":c]]]

	parsedef :: Type [String] -> [String]
	parsedef t c = ctypename t [" ":parsefun t ["(uint8_t (*get)(void), void *(*alloc)(size_t)":pks (typeKind t) True [")":c]]]

	pks :: Kind Bool [String] -> [String]
	pks k tl c = foldr (\(i, k) c->pd k tl i c) c $ zip2 [0..] $ typeArgs k

	pd :: Kind Bool Int [String] -> [String]
	pd KStar tl i c = [", void *(*", if tl ("parse_"+++toString i) "", ")(uint8_t (*)(void), void *(*)(size_t))":c]
	pd (l KArrow r) tl i c =
		[ ", void *(*", if tl ("parse_"+++toString i) "", ")(uint8_t (*)(void), void *(*)(size_t)"
		: pks l False $ pd r False (inc i) [")":c]]

	typeArgs :: Kind -> [Kind]
	typeArgs KStar = []
	typeArgs (l KArrow r) = [l:typeArgs r]

	parsergroup :: [Type] -> TPMonad ()
	parsergroup ts
		=   putPrefixMap [(typeName ty, (prefix ty, True))\\ty<-ts]
		>>| mapM_ funs ts
		>>| putPrefixMap [(typeName ty, (prefix ty, False))\\ty<-ts]
	where
		funs t = tell (parsenameimp t "r;" ctypename parsedef) >>| parser t >>| tell ["\n":tail]
			>>| tell (parsenameimp t (declaration t) ctypename parsedef_p) >>| parser_p t ["\n":tail]
		declaration t = concat ["*r = (":ctypename t [" *)alloc(sizeof(":ctypename t ["));"]]]
		parser_p t c = tell ["\t*r = ":parsefun t ["(get, alloc"
			:foldr (\i c->[", parse_", toString i:c]) [");\n":c]
				$ indexList $ typeArgs $ typeKind t]]
		putPrefixMap list = liftT $ modify $ withPrefixMap $ 'Data.Map'.putList list

	isPointer tname = liftT $ gets $ maybe False snd o 'Data.Map'.get tname o prefixMap

	printTypeName :: String -> TPMonad ()
	printTypeName tname = isPointer tname >>= \ip->tell [safe tname:if ip [" *"] []]

	parser :: Type -> TPMonad ()
	parser t | 'Data.Map'.member (typeName t) tmap
		= mapM_ (\x->tell ["\t", x]) $ 'Data.Map'.find (typeName t) tmap "r"
	parser t=:(TyRef _) = tell $ parsefun t []
	parser (TyBasic t)
		= case t of
			BTInt = parseint (\i->tell ["\t":i]) (IF_INT_64_OR_32 8 4) (IF_INT_64_OR_32 "int64_t" "int32_t") "r"
			BTChar = tell ["\tr = (Char)get();\n"]
			BTBool = tell ["\tr = (Bool)get();\n"]
			BTReal = parse64bitdouble (\i->tell ["\t":i]) "r"
			t = fail $ "parser: there is no basic type for " +++ toString t
	parser (TyArrow _ _) = fail $ "parser: function cannot be serialized"
	parser (TyNewType ti ci a) = fmtFields 1 ci.gcd_type ["r"]
	parser t=:(TyArray _ a)
		=   parseint (\i->tell ["\t":i]) 4 "uint32_t" "r.size"
		>>| tell ["\tr.elements = alloc(r.size*sizeof(",prefix a] >>| printTypeName (typeName a) >>| tell ["));\n"
			, "\tfor (uint32_t i = 0; i<r.size; i++) {\n"]
		>>| fmtFields 2 (typeGenType t) ["r.elements[i]"]
		>>| tell ["\t}\n"]
	parser (TyRecord ti fs)
		= fmtFields 1 ti.grd_type ["r" <-> safe fi.gfd_name\\(fi, _)<-fs]
	//Enumeration
	parser (TyObject ti fs)
		| and [t =: [] \\ (_, t)<-fs]
			= tell ["\tr = (", consName ti, ") get();\n"]
	//Single constructor, single field (box)
	parser (TyObject ti [(ci, [ty])])
		= fmtFields 1 ci.gcd_type ["r"]
	//Single constructor
	parser t=:(TyObject ti [(ci, ts)])
		= fmtFields 1 ci.gcd_type ["r" <-> "f" +++ toString i\\i<-indexList ts]
	//Complex adt
	parser (TyObject ti fs)
		=   tell ["\tr" <-> "cons = (", consName ti, "_c) get();\n"]
		>>| tell ["\tswitch(r" <-> "cons) {\n"]
		>>| mapM_ fmtCons fs
		>>| tell ["\t}\n"]
	where
		fmtCons :: (GenericConsDescriptor,[Type]) -> TPMonad ()
		fmtCons (ci, ts) = tell ["\tcase ", safe ci.gcd_name, "_c:\n"]
			>>| fmtFields 2 ci.gcd_type [cs i\\i<-[0..] & ty<-ts]
			>>| tell ["\t\tbreak;\n"]
		where
			cs i = "r" <-> "data" <.> safe ci.gcd_name +++ if (ts=:[_]) "" ("" <.> "f" +++ toString i)
	parser t = fail $ "parser: unsupported type " +++ toString t

	fmtFields :: Int GenType [String] -> TPMonad ()
	fmtFields i _ [] = pure ()
	fmtFields i (GenTypeArrow l r) [x:xs]
		= tell [createArray i '\t', x, " = "] >>| fmtField l True [] >>= tell >>| tell [");\n"] >>| fmtFields i r xs

	fmtField :: GenType Bool [String] -> TPMonad [String]
	fmtField (GenTypeApp (GenTypeCons a) (GenTypeCons t)) tl c
		| isMember a ["_Array", "_#Array", "_!Array"]
			= fmtField (GenTypeCons (t +++ a)) tl c
	fmtField (GenTypeCons a) tl c = isPointer a
		>>= \ip->pure ["parse_", safe a, if tl (if ip "_p" "" +++ "(get, alloc") "_p":c]
	fmtField (GenTypeVar a) tl c = pure ["parse_", toString a, if tl "(get, alloc" "":c]
	fmtField t=:(GenTypeApp _ _) tl c = ufold t c
	where
		ufold (GenTypeApp l r) c = fmtField r False c >>= \c->ufold l [", ":c]
		ufold t c = fmtField t tl c
	//Arrow are allowed, but only if it is an overridden type
	fmtField t=:(GenTypeArrow l r) tl c
		| isPredef r = fmtField r tl c
	where
		isPredef (GenTypeArrow l r) = isPredef r
		isPredef (GenTypeCons c) = 'Data.Map'.member c tmap
		isPredef _ = False
	fmtField t ts c = abort ("parser:fmtField doesn't match for: " +++ toString t)
