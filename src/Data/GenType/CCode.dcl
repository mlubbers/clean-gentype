definition module Data.GenType.CCode

from StdGeneric import :: GenericTypeDefDescriptor
from StdOverloaded import class zero
from Data.Either import :: Either

from Data.Map import :: Map
from Data.GenType import :: Box, :: GType, :: Type, generic gType

:: GenCCodeOptions =
	{ basename   :: String
	, genParser  :: Bool
	, genPrinter :: Bool
	, customDeps :: [[Type]] -> [[Type]]
	, overrides  :: [(String, [String], String -> [String], String -> [String])]
	}
instance zero GenCCodeOptions

generateCCode :: GenCCodeOptions (Box GType a) -> Either String ([String], [String]) | gType{|*|} a

/**
 * State of the generation monads
 *
 * @param Map from type to prefix and pointeryness
 * @param Accumulation of enumerations
 */
:: CCState =
	{ prefixMap :: Map String (String, Bool)
	, enums     :: [String]
	}
instance zero CCState
withPrefixMap :: ((Map String (String, Bool)) -> Map String (String, Bool)) CCState -> CCState
prefixMap :: CCState -> Map String (String, Bool)

/**
 * Create a C-safe type name
 */
safe :: String -> String

/**
 * Return the C type prefix, e.g. struct, enum
 */
prefix :: Type -> String

/**
 * Return the C constructorname
 */
consName :: GenericTypeDefDescriptor -> String
