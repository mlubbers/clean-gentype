implementation module Data.GenType.CSerialise

import Data.Array
import Data.Either
import StdEnv
import StdGeneric
import Data.Functor
import Control.GenBimap
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans

derive binumap StateT, Either
put a c = [a:c]

gCSerialise{|UNIT|} _ = id
gCSerialise{|EITHER|} f _ (LEFT x) = f x
gCSerialise{|EITHER|} _ f (RIGHT x) = f x
gCSerialise{|PAIR|} fl fr (PAIR l r) = fl l o fr r
gCSerialise{|CONS of {gcd_type_def,gcd_index}|} f (CONS x)
	//Enum
	| and [c.gcd_arity == 0\\c<-gcd_type_def.gtd_conses]
		= put gcd_index o f x
	//Single constructor
	| gcd_type_def.gtd_num_conses == 1 = f x
	= put gcd_index o f x
gCSerialise{|FIELD|} f (FIELD x) = f x
//Newtypes automatically work
gCSerialise{|OBJECT|} f (OBJECT x) = f x
gCSerialise{|RECORD|} f (RECORD x) = f x
gCSerialise{|Int|} x = \c->serialiseInt (IF_INT_64_OR_32 8 4) True x c
gCSerialise{|Bool|} x = put (if x 1 0)
gCSerialise{|Real|} x = \c->IF_INT_64_OR_32
	(toBytes 8 (convert_double_to_double_in_int x))
	(let (x1, x2) = convert_double_to_double_in_int32 x
	 in  toBytes 4 x1 o toBytes 4 x2)
	c

convert_double_to_double_in_int :: !Real -> Int
convert_double_to_double_in_int r = code { no_op }

convert_double_to_double_in_int32 :: !Real -> (!Int, !Int)
convert_double_to_double_in_int32 _ = code { no_op }

gCSerialise{|Char|} x = put (toInt x)
gCSerialise{|{}|} f x = serialiseArray f x
gCSerialise{|{!}|} f x = serialiseArray f x

serialiseInt :: Int Bool -> (a [Int] -> [Int]) | toInt a
serialiseInt bytes twocomp = \num->toBytes bytes (if twocomp (to2comp (bytes*8)) id (toInt num))

serialiseInt` :: Int Bool -> (Int [Int] -> [Int])
serialiseInt` bytes twocomp = serialiseInt bytes twocomp

serialiseArray :: (a [Int] -> [Int]) -> ((arr a) [Int] -> [Int]) | Array arr a
serialiseArray f = \x c->serialiseInt 4 False (size x) (foldrArr f c x)

toBytes :: Int Int [Int] -> [Int]
toBytes bytes i acc = [i >> (b*8) bitand 0xff\\b<-reverse [0..bytes-1]] ++ acc

to2comp :: Int Int -> Int
to2comp bits i = if (i < 0) (2 ^ bits + i) i

derive gCSerialise (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)
derive gCSerialise [], [! ], [ !], [!!], ?, ?^

serialiseToFile :: a *File -> *File | gCSerialise{|*|} a
serialiseToFile t f = foldr fwritec f (map toChar (gCSerialise{|*|} t []))

listTop :: CDeserialiser [Int] Int
listTop = StateT \x->case x of
	[] = Left CDInputExhausted
	[x:xs] = Right (x, xs)

sfileTop :: CDeserialiser File Int
sfileTop = StateT \f
	# (ok, c, f) = sfreadc f
	| not ok = Left CDInputExhausted
	= Right (toInt c, f)

instance toString CDeserialiseError
where
	toString CDInputExhausted = "Input exhausted"
	toString (CDUnknownConstructor s i) = "Unknown constructor in type " +++ s +++ " (" +++ toString i +++ ")"
	toString (CDUser e) = "User error: " +++ e

fail :: CDeserialiseError -> CDeserialiser st a
fail e = liftT (Left e)

(<<|>) infixl 3 :: (CDeserialiser st a) (CDeserialiser st a) -> CDeserialiser st a
(<<|>) l r = StateT \st->case runStateT l st of
	Left CDInputExhausted = Left CDInputExhausted
	Left e = runStateT r st
	Right (a, st) = Right (a, st)

gCDeserialise{|UNIT|} top = pure UNIT
gCDeserialise{|EITHER|} fl fr top = LEFT <$> fl top <<|> RIGHT <$> fr top
gCDeserialise{|PAIR|} fl fr top = PAIR <$> fl top <*> fr top
gCDeserialise{|CONS of {gcd_type_def,gcd_index}|} f top
	//Enumeration
	| and [c.gcd_arity == 0\\c<-gcd_type_def.gtd_conses]
		= top >>= \c->if (c == gcd_index) (CONS <$> f top) (fail (CDUnknownConstructor gcd_type_def.gtd_name c))
	//Single constructor
	| gcd_type_def.gtd_num_conses == 1 = CONS <$> f top
	= top >>= \c->if (c == gcd_index) (CONS <$> f top) (fail (CDUnknownConstructor gcd_type_def.gtd_name c))
gCDeserialise{|FIELD|} f top = (\x->FIELD x) <$> f top
gCDeserialise{|OBJECT|} f top = (\x->OBJECT x) <$> f top
gCDeserialise{|RECORD|} f top = RECORD <$> f top
gCDeserialise{|Int|} top = deserialiseInt (IF_INT_64_OR_32 8 4) True top
gCDeserialise{|Bool|} top = ((==)1) <$> top
gCDeserialise{|Real|} top = IF_INT_64_OR_32
	(convert_double_in_int_to_double <$> gCDeserialise{|*|} top)
	(curry convert_double_in_int_to_double32 <$> gCDeserialise{|*|} top <*> gCDeserialise{|*|} top)

convert_double_in_int_to_double :: !Int -> Real
convert_double_in_int_to_double r = code { no_op }

convert_double_in_int_to_double32 :: !(!Int, !Int) -> Real
convert_double_in_int_to_double32 _ = code {
		.d 0 2 r
			jmp _convert_double_in_int_to_double32_
		.o 0 2 r
			:_convert_double_in_int_to_double32_
	}

gCDeserialise{|Char|} top = toChar <$> top
gCDeserialise{|{}|} f top = (\a->{a\\a<-a}) <$> deserialiseN 0 (deserialiseInt 4 False) f top
gCDeserialise{|{!}|} f top = (\a->{a\\a<-a}) <$> deserialiseN 0 (deserialiseInt 4 False) f top

fro2comp :: Int Int -> Int
fro2comp bits i = let mask = 2 ^ dec bits in ~(i bitand mask) + (i bitand bitnot mask)

fromBytes :: [Int] -> Int
fromBytes s = sum [toInt c << (b*8)\\c<-s & b<-reverse [0..length s - 1]]

deserialiseInt :: Int Bool (CDeserialiser st Int) -> CDeserialiser st a | fromInt a
deserialiseInt bytes twocomp top
	= fromInt o if twocomp (fro2comp (bytes*8)) id o fromBytes <$> sequence [top\\p<-[0..bytes-1]]

deserialiseInt` :: Int Bool (CDeserialiser st Int) -> CDeserialiser st Int
deserialiseInt` bytes twocomp top = deserialiseInt` bytes twocomp top

derive gCDeserialise (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)
derive gCDeserialise [], [! ], [ !], [!!], ?, ?^

deserialiseN ::
	a 
	((CDeserialiser st Int) -> CDeserialiser st a)
	((CDeserialiser st Int) -> CDeserialiser st el)
	(CDeserialiser st Int)
	-> CDeserialiser st [el] | toInt a
deserialiseN _ psize pel top = psize top >>= \sz->sequence [pel top\\p<-[0..toInt sz-1]]

deserialiseMultiple :: (CDeserialiser st Int) [a] -> CDeserialiser st [a] | gCDeserialise{|*|} a
deserialiseMultiple top acc = StateT \st->case runStateT (gCDeserialise{|*|} top) st of
	Right (el, st) = runStateT (deserialiseMultiple top [el:acc]) st
	Left CDInputExhausted = Right (reverse acc, st)
	Left e = Left e
