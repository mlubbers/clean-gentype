definition module Data.GenType.CSerialise

from _SystemArray import class Array
import StdGeneric
from StdOverloaded import class toString, class toInt, class fromInt
from Data.Either import :: Either
from Control.Monad.State import :: StateT

// Serialisation
generic gCSerialise a :: a -> ([Int] -> [Int])
derive gCSerialise UNIT, EITHER, PAIR, CONS of {gcd_type_def,gcd_index}, FIELD, OBJECT, RECORD
derive gCSerialise Int, Bool, Real, Char
derive gCSerialise ?, ?^
derive gCSerialise [], [! ], [ !], [!!], {}, {!}
derive gCSerialise (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)

/**
 * Serialise an array (first output size, then the elements)
 *
 * @param serialiser for the elements
 * @result serialiser for the array
 */
serialiseArray :: (a [Int] -> [Int]) -> ((arr a) [Int] -> [Int]) | Array arr a
/**
 * Serialise an integer
 *
 * @param width of the int in bytes
 * @param twos complement
 * @result serialiser for the integer 
 */
serialiseInt :: Int Bool -> (a [Int] -> [Int]) | toInt a
//** serialiseInt but with a fixed return type
serialiseInt` :: Int Bool -> (Int [Int] -> [Int])

//* Deserialisation from a file
serialiseToFile :: a *File -> *File | gCSerialise{|*|} a

//* Top function for parsing from a list of bytes
listTop :: CDeserialiser [Int] Int

//* Top function for parsing from a read-only file
sfileTop :: CDeserialiser File Int

:: CDeserialiseError = CDInputExhausted | CDUnknownConstructor String Int | CDUser String
instance toString CDeserialiseError

:: CDeserialiser st a :== StateT st (Either CDeserialiseError) a
derive binumap StateT, Either
fail :: CDeserialiseError -> CDeserialiser st a
//* Alternative that takes into account CDInputExhausted errors
(<<|>) infixl 3 :: (CDeserialiser st a) (CDeserialiser st a) -> CDeserialiser st a
generic gCDeserialise a *! :: (CDeserialiser st Int) -> CDeserialiser st a
derive gCDeserialise UNIT, EITHER, PAIR, CONS of {gcd_type_def,gcd_index}, FIELD, OBJECT, RECORD
derive gCDeserialise Int, Bool, Real, Char
derive gCDeserialise ?, ?^
derive gCDeserialise [], [! ], [ !], [!!], {}, {!}
derive gCDeserialise (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)

/**
 * deserialise an integer
 *
 * @param width of the integer in bytes
 * @param twos complement
 * @param top
 * @result deserialiser
 */
deserialiseInt :: Int Bool (CDeserialiser st Int) -> CDeserialiser st a | fromInt a
//* deserialiseInt but with a fixed return type
deserialiseInt` :: Int Bool (CDeserialiser st Int) -> CDeserialiser st Int

/**
 * Deserialise size prefixed number of elements
 *
 * @param bogus value to easily force type checking
 * @param parse size
 * @param parse element
 * @param top
 * @result list parser
 */
deserialiseN ::
	a 
	((CDeserialiser st Int) -> CDeserialiser st a)
	((CDeserialiser st Int) -> CDeserialiser st el)
	(CDeserialiser st Int)
	-> CDeserialiser st [el] | toInt a

/**
 * Deserialise until the input is exhausted. This fails if after this list
 * another type comes.
 *
 * @param top
 * @param accumulator
 * @result list parser
 */
deserialiseMultiple :: (CDeserialiser st Int) [a] -> CDeserialiser st [a] | gCDeserialise{|*|} a
