definition module Data.VoidPointer

/**
 * Voidpointer type for C code generation.
 */

from Data.Either import :: Either
from Data.GenType import generic gType, :: Box, :: GType
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError, :: CDeserialiser, :: StateT
from Data.Either import :: Either

//** Datatype, it can contain data but only from C to C, not from Clean to C or
//** the other way around.
:: VoidPointer =: VoidPointer ()
/**
 * Constructor of the newtype that can be used curried
 *
 * @type VoidPointer
 */
vp :== VoidPointer ()
derive gType VoidPointer
derive gCSerialise VoidPointer
derive gCDeserialise VoidPointer

//* Helper function for C-code generation of void pointers (see {{gCSerialise}})
voidPointergType :: (String, [String], String -> [String], String -> [String])
