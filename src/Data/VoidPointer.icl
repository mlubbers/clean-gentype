implementation module Data.VoidPointer

import Control.Applicative
import Control.Monad
import Control.Monad.State
import Data.Either
import Data.Functor
import StdEnv

import Data.GenType
import Data.GenType.CSerialise

derive gType VoidPointer
gCSerialise{|VoidPointer|} _ = \x->[0:x]
gCDeserialise{|VoidPointer|} top = top >>= \w->vp <$ deserialiseInt` w False top

voidPointergType :: (String, [String], String -> [String], String -> [String])
voidPointergType =
	( gTypeName (gTypeForValue (VoidPointer ()))
	, ["typedef void *VoidPointer;\n"]
	, \r->
		[ "uintptr_t t = 0;\n"
		, "uint8_t w = get();\n"
		, "switch (w) {\n"
		, "\tcase 8:\n"
		, "\t\tt += ((uintptr_t)get())<<56;\n"
		, "\t\tt += ((uintptr_t)get())<<48;\n"
		, "\t\tt += ((uintptr_t)get())<<40;\n"
		, "\t\tt += ((uintptr_t)get())<<32;\n"
		, "\t/* fall-through */\n"
		, "\tcase 4:\n"
		, "\t\tt += ((uintptr_t)get())<<24;\n"
		, "\t\tt += ((uintptr_t)get())<<16;\n"
		, "\t/* fall-through */\n"
		, "\tcase 2:\n"
		, "\t\tt += ((uintptr_t)get())<<8;\n"
		, "\t\tt += ((uintptr_t)get());\n"
		, "\t/* fall-through */\n"
		, "\tcase 0:\n"
		, "\t\tbreak;\n"
		, "}\n"
		, r, " = (void *)t;\n"]
	, \r->
		[ "size_t w = sizeof(void*);\n"
		, "uintptr_t t = (uintptr_t)", r, ";\n"
		, "put(w);\n"
		, "switch(w) {\n"
		, "\tcase 8:\n"
		, "\t\tput((t>>56) & 0xFF);\n"
		, "\t\tput((t>>48) & 0xFF);\n"
		, "\t\tput((t>>40) & 0xFF);\n"
		, "\t\tput((t>>32) & 0xFF);\n"
		, "\t/* fall-through */\n"
		, "\tcase 4:\n"
		, "\t\tput((t>>24) & 0xFF);\n"
		, "\t\tput((t>>16) & 0xFF);\n"
		, "\t/* fall-through */\n"
		, "\tcase 2:\n"
		, "\t\tput((t>>8 ) & 0xFF);\n"
		, "\t\tput(t & 0xFF);\n"
		, "\t/* fall-through */\n"
		, "\tcase 0:\n"
		, "\t\tbreak;\n"
		, "}"])
