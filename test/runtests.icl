module runtests

import StdEnv, StdDebug
import StdOverloadedList

import Control.Monad.State
import Control.GenBimap
import Data.Either
import Data.Func
import Data.Functor
import Data.Int
import Data.Tuple
import Data.Maybe
import Data.GenEq
import Gast
import Gast.Gen
import System._Pointer
import System._Posix
import Text
import Text.GenPrint

import Data.GenType.CSerialise
import Data.VoidPointer

import Types

import code from "cp.o"
import code from "cruntests.o"

derive gEq CP, NT, ER, Enum, RA, UInt8, UInt16, Int8, Int16, Odd, Even, Tr, P, T, SR, VoidPointer
derive class Gast CP, NT, ER, Enum, RA, Tr, P, T, SR, VoidPointer
derive genShow UInt8, UInt16, Int8, Int16, Odd, Even
derive gPrint UInt8, UInt16, Int8, Int16, Odd, Even
ggen{|UInt8|} s = [!UInt8 i\\i<-[0, 255:[1..254]]]
ggen{|UInt16|} s = [!UInt16 i\\i<-[0, 65535:[1..65534]]]
ggen{|Int8|} s = [!Int8 i\\i<-[-128,127:[-127..126]]]
ggen{|Int16|} s = [!Int16 i\\i<-[-32768, 32767:[-32767..32765]]]
ggen{|Odd|} a s = [!OddBlurp,Odd EvenBlurp,Odd (Even (OddBlurp))]
ggen{|Even|} a s = [!EvenBlurp,Even OddBlurp,Even (Odd (EvenBlurp))]

testVoidPointer :: !*World -> *(!Bool, !*World)
testVoidPointer _ = code { ccall testVoidPointer ":I:A" }
ppUInt8 :: !Int !String !*World -> *(!Pointer, !*World)
ppUInt8 _ _ _ = code { ccall pp_UInt8 "Is:p:A" }
ppInt16 :: !Int !String !*World -> *(!Pointer, !*World)
ppInt16 _ _ _ = code { ccall pp_Int16 "Is:p:A" }
ppInt8 :: !Int !String !*World -> *(!Pointer, !*World)
ppInt8 _ _ _ = code { ccall pp_Int8 "Is:p:A" }
ppUInt16 :: !Int !String !*World -> *(!Pointer, !*World)
ppUInt16 _ _ _ = code { ccall pp_UInt16 "Is:p:A" }
ppInt :: !Int !String !*World -> *(!Pointer, !*World)
ppInt _ _ _ = code { ccall pp_Int "Is:p:A" }
ppBool :: !Int !String !*World -> *(!Pointer, !*World)
ppBool _ _ _ = code { ccall pp_Bool "Is:p:A" }
ppChar :: !Int !String !*World -> *(!Pointer, !*World)
ppChar _ _ _ = code { ccall pp_Char "Is:p:A" }
pp_Unit :: !Int !String !*World -> *(!Pointer, !*World)
pp_Unit _ _ _ = code { ccall pp__Unit "Is:p:A" }
ppEnum :: !Int !String !*World -> *(!Pointer, !*World)
ppEnum _ _ _ = code { ccall pp_Enum "Is:p:A" }
ppER :: !Int !String !*World -> *(!Pointer, !*World)
ppER _ _ _ = code { ccall pp_ER "Is:p:A" }
ppNT :: !Int !String !*World -> *(!Pointer, !*World)
ppNT _ _ _ = code { ccall pp_NT "Is:p:A" }
ppCP :: !Int !String !*World -> *(!Pointer, !*World)
ppCP _ _ _ = code { ccall pp_CP "Is:p:A" }
ppReal :: !Int !String !*World -> *(!Pointer, !*World)
ppReal _ _ _ = code { ccall pp_Real "Is:p:A" }

ppTupleBoolBool :: !Int !String !*World -> *(!Pointer, !*World)
ppTupleBoolBool _ _ _ = code { ccall pp__TupleBoolBool "Is:p:A" }

ppArrayBool :: !Int !String !*World -> *(!Pointer, !*World)
ppArrayBool _ _ _ = code { ccall pp_Bool_Array "Is:p:A" }

runTests :: !(Int String *World -> *(Pointer, *World)) ![!a] !*World -> *World | gCSerialise{|*|}, gCDeserialise{|*|}, Gast, gEq{|*|} a
runTests pp els w = foldl ffun w (take 10000 [e\\e<|-els])
where
	//Needs strictness to not overflows heap
	ffun :: !*World !a -> *World | gCSerialise{|*|}, gCDeserialise{|*|}, Gast, gEq{|*|} a
	ffun w el
		# msg = toString (gCSerialise{|*|} el [])
		# (p, w) = pp (size msg) msg w
		# newmsg = derefCharArray p (size msg)
		| newmsg <> msg = abort
			(   "Unequal serialised values for: " +++ printToString el +++ "\n"
			+++ "gave: " +++ join ", " [toString (toInt i)\\i<-:msg] +++ "\n"
			+++ "got:  " +++ join ", " [toString (toInt i)\\i<-:newmsg] +++ "\n"
			)
		= case runStateT (gCDeserialise{|*|} listTop) [toInt c\\c<-:newmsg] of
			Left e = abort ("Error deserialising: " +++ toString e +++ "\n")
			Right (a, [])
				| a === el = w
				= abort
					(   "Unequal deserialised values\n"
					+++ "gave:  " +++ printToString el +++ "\n"
					+++ "got: " +++ printToString a +++ "\n"
					)
			Right _ = abort
					(   "Input exhausted: \n"
					+++ "val: " +++ printToString el +++ "\n"
					+++ "got: " +++ printToString newmsg +++ "\n"
					)

testM :: !String ![Int] !(![a], ![Int]) !*World -> *World | gEq{|*|}, gCDeserialise{|*|} a
testM msg input expected world = case runStateT (deserialiseMultiple listTop []) input of
	Left e = abort (msg +++ ": " +++ toString e +++ "\n")
	Right v
		| not (v === expected) = abort (msg +++ "\n")
		| otherwise = world

testMultiple world
	# mr = gCSerialise{|*|} (CMutrec OddBlurp)
	= testM "multiple CP" (mr [5]) ([CMutrec OddBlurp], [5])
	$ testM "multiple CP" (mr []) ([CMutrec OddBlurp], [])
	$ testM "multiple CP" (mr $ mr []) ([CMutrec OddBlurp,CMutrec OddBlurp], [])
	$ testM "multiple CP" (mr $ mr [3]) ([CMutrec OddBlurp,CMutrec OddBlurp], [3])
	$ testM "multiple uint16" [0,42,0] ([UInt16 42], [0])
	$ testM "multiple uint16" [0,42,0,38,0] ([UInt16 42,UInt16 38], [0])
	$ testM "multiple uint16" [0,42,0,38] ([UInt16 42,UInt16 38], [])
	$ testM "multiple uint16" [] (tl [UInt16 0], [])
	$ world

Start w = id
	$ (\w->let (ok, w`) = testVoidPointer w in (if ok w` (abort "testVoidPointer failed\n")))
	$ runTests ppUInt8 [!UInt8 0:ggen{|*|} genState]
	$ runTests ppUInt16 [!UInt16 0:ggen{|*|} genState]
	$ runTests ppBool [!True:ggen{|*|} genState]
	$ runTests ppInt [!0:ggen{|*|} genState]
	$ runTests ppChar [!'\0':ggen{|*|} genState]
	$ runTests ppReal (Filter (not o isNaN) [!0.0:ggen{|*|} genState])
	$ runTests pp_Unit [!():ggen{|*|} genState]
	$ runTests ppEnum [!A:ggen{|*|} genState]
	$ runTests ppER [!{nat=1,boolean=True}:ggen{|*|} genState]
	$ runTests ppNT [!NT 42:ggen{|*|} genState]
	$ runTests ppTupleBoolBool [!(True, True):ggen{|*|} genState]
	$ runTests ppArrayBool [!{!True}:ggen{|*|} genState]
	$ runTests ppCP (Filter notnan [!CLeft {a1='\0',a2=5} [True] 0 ():ggen{|*|} genState])
	$ testMultiple
	$ w

notnan (CNum _ _ _ _ x) = x == x
notnan _ = True
