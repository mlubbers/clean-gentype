implementation module Types

import Control.Applicative
import Data.Either
import Data.Func
import Data.Functor
import Data.Tuple
import StdEnv

import Data.GenType
import Data.GenType.CSerialise
import Data.VoidPointer

derive gType T, R, Frac, Tr, Fix, Odd, Even, SR, List, Enum, NT, Blurp, EnumList, ER, CP, RA, Nest, MR, P, UInt8, UInt16, Int8, Int16, Either
derive gCSerialise CP, NT, ER, Enum, RA, Odd, Even, Either, Tr, P, EnumList, T, SR
gCSerialise{|UInt8|} i = serialiseInt 1 False i
gCSerialise{|UInt16|} i = serialiseInt 2 False i
gCSerialise{|Int8|} i = serialiseInt 1 True i
gCSerialise{|Int16|} i = serialiseInt 2 True i
derive gCDeserialise CP, NT, ER, Enum, RA, Odd, Even, Either, Tr, P, EnumList, T, SR
gCDeserialise{|UInt8|} top = deserialiseInt 1 False top
gCDeserialise{|UInt16|} top = deserialiseInt 2 False top
gCDeserialise{|Int8|} top = deserialiseInt 1 True top
gCDeserialise{|Int16|} top = deserialiseInt 2 True top

instance toInt UInt8 where toInt (UInt8 i) = i
instance toInt UInt16 where toInt (UInt16 i) = i
instance toInt Int8 where toInt (Int8 i) = i
instance toInt Int16 where toInt (Int16 i) = i
instance fromInt UInt8 where fromInt i = UInt8 (min 0xff (max 0 i))
instance fromInt UInt16 where fromInt i = UInt16 (min 0xffff (max 0 i))
instance fromInt Int8 where fromInt i = Int8 (min 0x7f (max -0x80 i))
instance fromInt Int16 where fromInt i = Int16 (min 0x7fff (max -0x8000 i))

uint8Typedef :: [String]
uint8Typedef = ["typedef uint8_t UInt8;\n"]

uint8Parser :: String -> [String]
uint8Parser r = [r +++ " = get();\n"]

uint8Printer :: String -> [String]
uint8Printer r = ["put(" +++ r +++ ");\n"]

int8Typedef :: [String]
int8Typedef = ["typedef int8_t Int8;\n"]

int8Parser :: String -> [String]
int8Parser r = [r +++ " = get();\n"]

int8Printer :: String -> [String]
int8Printer r = ["put(" +++ r +++ ");\n"]

uint16Typedef :: [String]
uint16Typedef = ["typedef uint16_t UInt16;\n"]

uint16Parser :: String -> [String]
uint16Parser r = [r +++ " = get()<<8;\n", r +++ " += get();\n"]

uint16Printer :: String -> [String]
uint16Printer r = ["put(" +++ r +++ ">>8);\n", "put(" +++ r +++ " & 0xff);\n"]

int16Typedef :: [String]
int16Typedef = ["typedef int16_t Int16;\n"]

int16Parser :: String -> [String]
int16Parser r = [r +++ " = get()<<8;\n", r +++ " += get();\n"]

int16Printer :: String -> [String]
int16Printer r = ["put(" +++ r +++ ">>8);\n", "put(" +++ r +++ " & 0xff);\n"]

cp :: Box GType (CP, VoidPointer)
cp = gType{|*|}
