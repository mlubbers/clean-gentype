#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include "cp.h"

uint8_t rmsg[1000];
uint8_t *msg;
int idx, msglen;
char *current = "none";

uint8_t get(void)
{
	if (idx == msglen) {
		fprintf(stderr, "Input exhausted in %s\n", current);
		exit(EXIT_FAILURE);
	}
	return msg[idx++];
}

void put(uint8_t c)
{
	if (idx == msglen) {
		fprintf(stderr, "Buffer overrun in %s\n", current);
		exit(EXIT_FAILURE);
	}
	msg[idx++] = c;
}

#define pp_(name, type, parse, print)\
uint8_t *name(int sz, uint8_t *newmsg) {\
	current = #name;\
	msg = newmsg;\
	idx = 0;\
	msglen = sz;\
	type r = parse(get, malloc);\
	idx = 0;\
	msg = &rmsg[0];\
	print(put, r);\
	return msg;\
}

pp_(pp_UInt8, UInt8, parse_UInt8, print_UInt8)
pp_(pp_UInt16, UInt16, parse_UInt16, print_UInt16)
pp_(pp_Int, Int, parse_Int, print_Int)
pp_(pp_Bool, Bool, parse_Bool, print_Bool)
pp_(pp_Char, Char, parse_Char, print_Char)
pp_(pp_Enum, enum Enum, parse_Enum, print_Enum)
pp_(pp_Real, Real, parse_Real, print_Real)
pp_(pp__Unit, enum _Unit, parse__Unit, print__Unit)
pp_(pp_NT, NT, parse_NT, print_NT)
pp_(pp_ER, struct ER, parse_ER, print_ER)
pp_(pp_CP, struct CP, parse_CP, print_CP)

uint8_t *pp_Bool_Array(int sz, uint8_t *newmsg) {
	current = "Bool_Array";
	msg = newmsg;
	idx = 0;
	msglen = sz;
	struct _Array r = parse__Array(get, malloc, parse_Bool_p);
	idx = 0;
	msg = &rmsg[0];
	print__Array(put, r, print_Bool_p);
	return msg;
}
uint8_t *pp__TupleBoolBool(int sz, uint8_t *newmsg) {
	current = "_TupleBoolBool";
	msg = newmsg;
	idx = 0;
	msglen = sz;
	struct _Tuple2 r = parse__Tuple2(get, malloc, parse_Bool_p, parse_Bool_p);
	idx = 0;
	msg = &rmsg[0];
	print__Tuple2(put, r, print_Bool_p, print_Bool_p);
	return msg;
}

bool testVoidPointer(void) {
	current = "testVoidPointer";
	msglen = 10;
	msg = malloc(msglen*sizeof(uint8_t));
	void *vp[] = {(void *)0, (void *)1, (void *)42, (void *)38};
	void *vp0;
	for (size_t i = 0; i<sizeof(vp)/sizeof(vp[0]); i++) {
		idx = 0;
		print_VoidPointer(put, vp[i]);
		idx = 0;
		vp0 = parse_VoidPointer(get, malloc);
		if (vp0 != vp[i]) {
			free(msg);
			return false;
		}
	}
	free(msg);

	// Test for smaller pointers
	bool ret = true;
	msg = (uint8_t []){0};
	idx = 0;
	vp0 = parse_VoidPointer(get, malloc);
	if ((uintptr_t)vp0 != 0) {
		fprintf(stderr, "test vp length 0\n");
		ret = false;
	}
	// Length 2
	msg = (uint8_t []){2,0,42};
	idx = 0;
	vp0 = parse_VoidPointer(get, malloc);
	if ((uintptr_t)vp0 != 42) {
		fprintf(stderr, "test vp length 2 (42), was %lu\n",
			(uintptr_t)vp0);
		ret = false;
	}

	msg = (uint8_t []){2,2,1};
	idx = 0;
	vp0 = parse_VoidPointer(get, malloc);
	if ((uintptr_t)vp0 != 513) {
		fprintf(stderr, "test vp length 2 (513), was %lu\n",
			(uintptr_t)vp0);
		ret = false;
	}
	msg = (uint8_t []){2,0,0};
	idx = 0;
	vp0 = parse_VoidPointer(get, malloc);
	if ((uintptr_t)vp0 != 0) {
		fprintf(stderr, "test vp length 2 (0), was %lu\n",
			(uintptr_t)vp0);
		ret = false;
	}
	// Length 4
	msg = (uint8_t []){4,0,0,0,42};
	idx = 0;
	vp0 = parse_VoidPointer(get, malloc);
	if ((uintptr_t)vp0 != 42) {
		fprintf(stderr, "test vp length 4 (42), was %lu\n",
			(uintptr_t)vp0);
		ret = false;
	}
	msg = (uint8_t []){4,4,3,2,1};
	idx = 0;
	vp0 = parse_VoidPointer(get, malloc);
	if ((uintptr_t)vp0 != 67305985) {
		fprintf(stderr, "test vp length 4 (67305985), was %lu\n",
			(uintptr_t)vp0);
		ret = false;
	}
	msg = (uint8_t []){4,0,0,0,0};
	idx = 0;
	vp0 = parse_VoidPointer(get, malloc);
	if ((uintptr_t)vp0 != 0) {
		fprintf(stderr, "test vp length 4 (0), was %lu\n",
			(uintptr_t)vp0);
		ret = false;
	}

	return ret;
}
