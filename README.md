# Deeply embedded generics

This library provides a deeply embedded representation for the generic type
structure.

Furthermore, it contains c-code generation for some datatypes.
Note that this code generation does not work for curried data types, functions,
and higher-kinded types.

## Maintainer

Mart Lubbers (mart@cs.ru.nl)

## License

`gentype` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).
