# Changelog

### v3.1.0

- Fix: deserialiseMultiple was broken
- Feature: expose <<|> as a safe alternative operator

#### v3.0.1

- Enhancement: Add required binumap exports for CDeserialise

## v3

- Chore: support base 3
- Feature: add VoidPointer type
- Optimisation: use standard monad types

#### v2.1.3

- Chore: support base 2.

#### v2.1.2

- fix instance of () again (now for arrays) (for mac clang)

#### v2.1.1

- fix instance of () again (for mac clang)

### v2.1

- reinline arrays again
- fix enum prototypes

## v2.0

- Use nitrile format version
- fix clang errors
- use union instead of struct (see #3)
- Stop inlining arrays (see #2)

### v1.1

- Bump version to support new platform (and thus itasks)

## v1.0

- Initial nitrile version
